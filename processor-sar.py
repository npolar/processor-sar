#!/usr/bin/env python3

import re
import geojson
from shapely import wkt
import argparse
import datetime
import glob
import os
import subprocess
from zipfile import ZipFile
import multiprocessing
from functools import partial
import logging
import shutil
import json
import sys
from sentinelsat import SentinelAPI, geojson_to_wkt
import rasterio
from pyproj import Proj
import affine
import numpy
from urllib import request as rq
import requests
import xmltodict
import time
import tempfile
from dateutil import parser as date_parser
import numpy as np
import cartopy.crs as ccrs
from cartopy import feature as cfeature
import rasterio
import matplotlib.pyplot as plt
from cartopy.mpl.gridliner import _lon_heimisphere, _lat_heimisphere
import matplotlib
import matplotlib.ticker as mticker


def convert_coords(coords_list, crs):
    src_proj = Proj(init='EPSG:4326')
    trg_proj = Proj(init=crs)
    xy_list = [trg_proj(c[0], c[1]) for c in coords_list[0]]
    x_array = numpy.array([c[0] for c in xy_list])
    y_array = numpy.array([c[1] for c in xy_list])
    return x_array, y_array


def poll_marine_traffic():
    api_key = os.environ['MARINETRAFFIC_API_KEY']
    mt_url = "https://services.marinetraffic.com/api/exportvessels/v:8/{}/protocol:jsono".format(api_key) 
    response = requests.get(mt_url)
    content = json.loads(response.text)[0]
    return content


def get_ais_position():
    """
    Poll marine traffic for ships position, tailored to single boat (KPH atm)
    returns lon, lat, heading, course, time
    """
    try:
        mt_dict = poll_marine_traffic()
    except:
        time.sleep(140) # sleep if need to repeat request to Marine Traffic
        mt_dict = poll_marine_traffic()

    res_dict= dict(lon=float(mt_dict['LON']),
                  lat=float(mt_dict['LAT']),
                  speed=float(mt_dict['SPEED'])/10.,
                  heading=int(mt_dict['HEADING']),
                  course=int(mt_dict['COURSE']),
                  timestamp=datetime.datetime.strptime(mt_dict['TIMESTAMP'], "%Y-%m-%dT%H:%M:%S")
                  )
    return res_dict

def buffer_over_boat(lon, lat, buffer_radius=100000.):
    """
    Create buffer over position
    """
    from shapely.geometry import Point
    import pyproj
    from shapely.ops import transform
    from functools import partial

    pr = pyproj.Proj(init="{}".format(request["crs"].lower()))
    project_func = partial(pyproj.transform,
                           pr,
                           pyproj.Proj(init="epsg:4326"))
    x, y = pr(lon, lat)
    pbuffer_xy = Point(x, y).buffer(buffer_radius, resolution=4, cap_style=3)
    pbuffer_geodetic = transform(project_func, pbuffer_xy)
    return pbuffer_geodetic

def create_empty_dst(fpath, coords_list, res, crs, dtype):
    a_pixel_width = res
    b_rotation = 0
    d_column_rotation = 0
    e_pixel_height = res

    crs = str(crs).upper()

    (x_array, y_array) = convert_coords(coords_list, crs)
    c_x_ul = x_array.min()
    f_y_ul = y_array.max()
    height = (y_array.max() - y_array.min()) / res
    width = (x_array.max() - x_array.min()) / res

    aff = affine.Affine(a_pixel_width,
                 b_rotation,
                 c_x_ul,
                 d_column_rotation,
                 -1 * e_pixel_height,
                 f_y_ul)

    dst = rasterio.open(fpath,
                        'w',
                        driver='GTiff',
                        height=height,
                        width=width,
                        count=1,
                        dtype=dtype,
                        crs=crs,
                        transform=aff,
                        nodata=0,
                        compress="LZW",
                        predict=2
                        )
    return dst

def percentile_peak(ifile):
    with rasterio.open(ifile, 'r') as dst:
        ubound = numpy.percentile(dst.read(1), 95)
    return ubound

def process_sentinel_scene(product, data_dir, output_filepath):
    """
    Extract the contents of the obtained file
    and put it in the right place
    """
    with ZipFile(os.path.join(data_dir, ".".join([product['identifier'], "zip"]))) as zf:
        zf.extractall(data_dir)
        input_paths = glob.glob(os.path.join(data_dir, ".".join([product['identifier'],"SAFE/measurement/*.tiff"])))
        for i, ifile in enumerate(input_paths):
            if re.search(r".*(vh).*.tiff|.*(hh).*.tiff", ifile):
                with tempfile.NamedTemporaryFile() as temp:
                    uboundval = percentile_peak(ifile)
                    subprocess.call([
                        "gdal_translate",
                        "-scale",
                        "0", "{}".format(int(uboundval)), "0", "255",
                        "-ot", "Byte",
                        ifile,
                        temp.name])

                    subprocess.call([
                        "gdalwarp",
                        "-t_srs",
                        request["crs"],
                        "-srcnodata",
                        "0",
                        "-dstnodata",
                        "0",
                        "-r",
                        "bilinear",
                        temp.name,
                        output_filepath
                    ])


def read_credentials(filepath):
    with open(filepath, 'r') as credentials:
        user = credentials.readline().rstrip('\n')
        password = credentials.readline().rstrip('\n')
        return user, password

def download_sentinel_product(
                        product_key,
                        products,
                        api,
                        request,
                        output_path
                    ):

    print("Obtaining item {}".format(product_key))
    api.download(product_key, directory_path=output_path)



def remove_dir(path):
    for root, dirs, files in os.walk(path):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))
        os.rmdir(root)


def plot_gtiff(fpath, format=None):
    """Don't forget to overwrite the geotiff when creating a plot"""
    import io

    matplotlib.rc('xtick', labelsize=2)
    matplotlib.rc('ytick', labelsize=2)

    _DEGREE_SYMBOL = u'\u00B0'

    def _east_west_formatted_minutes(longitude, num_format='d', num_format_minutes='02.2f'):
        fmt_string = u'{longitude:{num_format}}{degree}{minutes:{num_format_minutes}}{minutes_symbol}{hemisphere}'
        minutes = np.mod(abs(longitude), 1)
        lon = int(abs(longitude) - minutes)
        minutes = minutes * 60
        return fmt_string.format(longitude=lon, num_format=num_format,
                                 minutes=minutes, num_format_minutes=num_format_minutes,
                                 minutes_symbol="'",
                                 hemisphere=_lon_heimisphere(longitude),
                                 degree=_DEGREE_SYMBOL)

    def _north_south_formatted_minutes(latitude, num_format='d', num_format_minutes='02.2f'):
        fmt_string = u'{latitude:{num_format}}{degree}{minutes:{num_format_minutes}}{minutes_symbol}{hemisphere}'
        minutes = np.mod(abs(latitude), 1)
        lon = int(abs(latitude) - minutes)
        minutes = minutes * 60
        return fmt_string.format(latitude=lon, num_format=num_format,
                                 minutes=minutes, num_format_minutes=num_format_minutes,
                                 minutes_symbol="'",
                                 hemisphere=_lat_heimisphere(latitude),
                                 degree=_DEGREE_SYMBOL)

    #: A formatter which turns longitude values into nice longitudes such as 110W
    LONGITUDE_FORMATTER_MINUTES = mticker.FuncFormatter(lambda v, pos:
                                                        _east_west_formatted_minutes(v))

    #: A formatter which turns longitude values into nice longitudes such as 45S
    LATITUDE_FORMATTER_MINUTES = mticker.FuncFormatter(lambda v, pos:
                                                       _north_south_formatted_minutes(v))

    with rasterio.drivers():
        with rasterio.open(fpath, 'r') as src:
            # read image into ndarray
            im = src.read(1)

            # calculate extent of raster
            xmin = src.transform[0]
            xmax = src.transform[0] + src.transform[1] * src.width
            ymin = src.transform[3] + src.transform[5] * src.height
            ymax = src.transform[3]

    # define cartopy crs for the raster, based on rasterio metadata
    # crs = ccrs.epsg(src.crs['init'].split(":")[1])
    # crs = ccrs.Stereographic(central_latitude=-90,
    #                          central_longitude=0,
    #                          true_scale_latitude=-71)
    crs = ccrs.Mercator()
    # +proj=stere +lat_0=-90 +lat_ts=-71 +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs
    # create figure

    dpi = 80
    height, width = im.shape

    # What size does the figure need to be in inches to fit the image?
    figsize = width / float(dpi), height / float(dpi)
    fig = plt.figure(figsize=figsize, frameon=False)
    # ax = plt.axes([0.0, 0.0, 1.0, 1.0], projection=crs)
    ax = plt.axes(projection=crs)

    # plot raster
    plt.imshow(np.ma.array(im, mask=im <= 1),
               origin='upper',
               extent=[xmin, xmax, ymin, ymax],
               transform=crs, interpolation='none', cmap='Greys_r')

    # plot coastlines
    ax.coastlines(resolution='10m', color='yellow', linewidth=2)
    gl = ax.gridlines(draw_labels=True)
    gl.xformatter = LONGITUDE_FORMATTER_MINUTES
    gl.yformatter = LATITUDE_FORMATTER_MINUTES
    gl.xlabel_style = {'size': 12, 'color': 'gray'}
    gl.ylabel_style = {'size': 12, 'color': 'gray'}

    if format:
        format = format.to_lower()

    # wave hands to prevent matplotlib creating new file with filename + format extension
    # instead overwriting the given path
    buf = io.BytesIO()
    fig.savefig(buf, format='jpeg', dpi=dpi, bbox_inches='tight', pad_inches=0.50)
    buf.seek(0)

    with open(fpath,'wb') as out: # Open temporary file as bytes
        out.write(buf.read())

def main():

    p = argparse.ArgumentParser()
    p.add_argument("--log-file", default=None)
    p.add_argument("--output-file", default=None)
    p.add_argument("--request-file", default=None)
    p.add_argument("--download-dir", default=None)
    p.add_argument("--cleanup-dir", default=True, action="store_true")
    p.add_argument("-k", "--keep-temporary", action="store_true")
    p.add_argument("--download-only", action="store_true")

    args = p.parse_args()

    SHUB_USER = os.environ['SHUB_USER']
    SHUB_PASS = os.environ['SHUB_PASS']

    global logger
    logger = logging.getLogger('mapmaker')
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s  %(name)s  %(levelname)s: %(message)s')

    file_handler = logging.FileHandler(args.log_file)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    if args.request_file is not None:
        global request
        request = json.loads(open(args.request_file).read())
    else:
        logger.info("Request file missing, aborting")
        raise Exception("Can't proceed without request json file")

    # api_url = 'https://colhub.met.no/'
    api_url = 'https://scihub.copernicus.eu/dhus/'
    api = SentinelAPI(SHUB_USER, SHUB_PASS, api_url=api_url, show_progressbars=True)

    # Figure what the request coordinates should be
    # Replace ROI AIS part with actual coordinates
    if request['roi'] == "ais":
        ais_dict = get_ais_position()
        center_lon = ais_dict['lon']
        center_lat = ais_dict['lat']
    elif request["roi"] == "bbox":
        center_lat = request["center_lat"]
        center_lon = request["center_lon"]

    try:
        bbox_radius = request["box_radius"]
    except:
        bbox_radius = 1e5

    try:
        logger.info("Object position: {} {}".format(center_lat, center_lon))
        footprint = buffer_over_boat(center_lon, center_lat, buffer_radius=bbox_radius).wkt
        as_geojson = geojson.Feature(geometry=wkt.loads(footprint), properties={})
        request['roi'] = as_geojson['geometry']
    except:
        footprint = geojson_to_wkt(request['roi'])
        logger.debug("Search footprint is: {}".format(footprint))

    footprint = geojson_to_wkt(request['roi'])
    logger.debug("Search footprint is: {}".format(footprint))

    try:
        if request['end_time']:
            end_time = date_parser.parse(request['end_time'])
    except:
        end_time = datetime.datetime.utcnow()

    start_time = end_time - datetime.timedelta(hours=request['time_delta_hours'])

    # query dhus
    products = api.query(
        footprint,
        date = (start_time, end_time),
        platformname=request['platformname'],
        producttype = request['producttype'],
        sensoroperationalmode=request['sensoroperationalmode']
    )

    # process quering results
    n_scenes = len(products.keys())
    message = "Found {} scenes".format(n_scenes)
    logger.info(message)
    if n_scenes>0:
        logger.info("Scenes list:\n{}".format(
            "\n".join(['\t'+products[key]['identifier'] for key in products.keys()])))
    else:
        logger.warn(" ".join(['Not enough scenes found',
                              'within last {} hours, aborting'.format(request["time_delta_hours"])]))
        sys.exit()

    output_path = args.output_file
    logger.debug("Output path: {}".format(output_path))
    if args.download_dir is None:
        data_dir = os.path.splitext(output_path)[0] + ".d"
    else:
        data_dir = args.download_dir

    logger.debug("Download data directory: {}".format(data_dir))

    if not os.path.exists(data_dir):
        os.mkdir(data_dir)

    logger.debug("Begin download")
    pool = multiprocessing.Pool(processes=1)
    output = pool.map(partial(download_sentinel_product,
                              products=products,
                              api=api,
                              output_path=data_dir,
                              request=request), products.keys())

    if args.download_only is True:
        logger.info("Asked to only download data, stopping")
        sys.exit()

    # Process obtained files with gdal
    with tempfile.NamedTemporaryFile() as tfile:

        create_empty_dst(
            tfile.name,
            request['roi']['coordinates'],
            request['spatial_resolution'],
            request['crs'],
            rasterio.uint8
        )

        for product_key in products.keys():
            process_sentinel_scene(products[product_key], data_dir, tfile.name)

        subprocess.call([
            "gdal_translate",
            "-ot", "Byte",
            "-co", "COMPRESS=JPEG",
            "-co", "JPEG_QUALITY=70",
            "-scale",
            tfile.name,
            output_path
            ])

    try:
        if request['plot_data'] == True:
            plot_gtiff(output_path)
    except KeyError:
        pass
    except ValueError:
        warnings.warn("Unknown value for key plot_data, not plotting")

    if args.keep_temporary is False:
        logger.info('Removing temporary directory {}'.format(data_dir))
        remove_dir(data_dir)

if __name__ == "__main__":
    main()
