# Sentinel-1 downloader and preview generator

## Installation

    git clone https://github.com/mitkin/processor-sar.git
	pip install -r requirements.txt
	
TKinter is currently a dependency, so please do:
	apt install python3-tk

## Usage:
`SHUB_USER=foo SHUB_PASS=bar ./processor-sar.py --request-file=sar-request.json --output-file=sar.tif --log-file=out.log --download-only --download-dir=data`
